

%infeasibility%G_INFES.fx(n,h) = 0 ;
%infeasibility%H_INFES.fx(n,bu,ch,h) = 0 ;
%infeasibility%H_DHW_INFES.fx(n,bu,ch,h) = 0 ;

Parameter
m_exog_p(n,tech)
m_exog_sto_e(n,sto)
m_exog_sto_p_in(n,sto)
m_exog_sto_p_out(n,sto)
m_exog_rsvr_p(n,rsvr)
m_exog_rsvr_e(n,rsvr)
m_exog_ntc(l)
;

m_exog_p(n,tech)        = technology_data(n,tech,'fixed_capacities') ;
m_exog_sto_e(n,sto)     = storage_data(n,sto,'fixed_capacities_energy');
m_exog_sto_p_in(n,sto)  = storage_data(n,sto,'fixed_capacities_power_in');
m_exog_sto_p_out(n,sto) = storage_data(n,sto,'fixed_capacities_power_out');
m_exog_rsvr_p(n,rsvr)   = reservoir_data(n,rsvr,'fixed_capacities_power');
m_exog_rsvr_e(n,rsvr)   = reservoir_data(n,rsvr,'fixed_capacities_energy');
m_exog_ntc(l)           = topology_data(l,'fixed_capacities_ntc');

*** Dispatch model
* %dispatch_only%$ontext
* N_TECH.fx(n,tech)      = m_exog_p(n,tech) ;
* N_STO_P_IN.fx(n,sto)   = m_exog_sto_p_in(n,sto) ;
* N_STO_P_OUT.fx(n,sto)  = m_exog_sto_p_out(n,sto) ;
* N_STO_E.fx(n,sto)      = m_exog_sto_e(n,sto) ;
* N_RSVR_P.fx(n,rsvr)    = m_exog_rsvr_p(n,rsvr) ;
* NTC.fx(l)              = m_exog_ntc(l) ;
* $ontext
* $offtext

*** Investment model
%investment%$ontext

***** Set bounds ***** 

%py_all_bounded%$ontext

N_TECH.fx(n,'ror')           = technology_data(n,'ror','min_installable') ;
N_TECH.fx(n,'bio')           = technology_data(n,'bio','min_installable') ;
N_STO_P_IN.fx(n,'phs')       = storage_data(n,'phs','min_power_in');
N_STO_P_OUT.fx(n,'phs')      = storage_data(n,'phs','min_power_out');
N_STO_E.fx(n,'phs')          = storage_data(n,'phs','min_energy');
N_STO_P_IN.fx(n,'phs_open')  = storage_data(n,'phs_open','min_power_in');
N_STO_P_OUT.fx(n,'phs_open') = storage_data(n,'phs_open','min_power_out');
N_STO_E.fx(n,'phs_open')     = storage_data(n,'phs_open','min_energy');
N_RSVR_P.fx(n,rsvr)          = reservoir_data(n,rsvr,'min_power') ;
N_RSVR_E.fx(n,rsvr)          = reservoir_data(n,rsvr,'min_energy') ;

* NTC.lo(l) = topology_data(l,'min_installable') ;
* NTC.up(l) = topology_data(l,'max_installable') ;

$ontext
$offtext

***** Set bounds dependend on constraints selected ****

%py_all_bounded%%py_demand_share_ror%N_TECH.fx(n,'ror')           = technology_data(n,'ror','min_installable') ;
%py_all_bounded%%py_demand_share_bio%N_TECH.fx(n,'bio')           = technology_data(n,'bio','min_installable') ;
%py_all_bounded%%py_demand_share_rsvr%N_RSVR_P.fx(n,rsvr)         = reservoir_data(n,rsvr,'min_power') ;
%py_all_bounded%%py_demand_share_rsvr%N_RSVR_E.fx(n,rsvr)         = reservoir_data(n,rsvr,'min_energy') ;
%py_all_bounded%%py_demand_share_phs%N_STO_P_IN.fx(n,'phs')       = storage_data(n,'phs','min_power_in');
%py_all_bounded%%py_demand_share_phs%N_STO_P_OUT.fx(n,'phs')      = storage_data(n,'phs','min_power_out');
%py_all_bounded%%py_demand_share_phs%N_STO_E.fx(n,'phs')          = storage_data(n,'phs','min_energy');
%py_all_bounded%%py_demand_share_phs%N_STO_P_IN.fx(n,'phs_open')  = storage_data(n,'phs_open','min_power_in');
%py_all_bounded%%py_demand_share_phs%N_STO_P_OUT.fx(n,'phs_open') = storage_data(n,'phs_open','min_power_out');
%py_all_bounded%%py_demand_share_phs%N_STO_E.fx(n,'phs_open')     = storage_data(n,'phs_open','min_energy');

NTC.fx(l) = m_exog_ntc(l) ;

*** No network transfer
%net_transfer%NTC.fx(l) = 0 ;
%net_transfer%F.fx(l,h) = 0 ;

***** Data iteration **********************************************************

%iter_data_switch%$ontext

Sets
scenario
identifer
;

Parameters
iter_data(h,identifer,scenario)
;

$GDXin "%data_it_gdx%"

$load scenario
$load identifer
$load iter_data
;

$ontext
$offtext

*******************************************************************************

*set used for report
Set feat_included(features);
feat_included(features) = yes$(sum(n, feat_node(features,n))) ;

* Define a set of countries that contains all countries except DE
* Set
* n_noDE(n)
* ;
* 
* n_noDE(n)    = yes;
* n_noDE('DE') = no;

*******************************************************************************

* Fill up 0 with eps

phi_res(n,'pv',h)       = phi_res(n,'pv',h)       + eps ;
phi_res(n,'wind_on',h)  = phi_res(n,'wind_on',h)  + eps ;
phi_res(n,'wind_off',h) = phi_res(n,'wind_off',h) + eps ;

* activate "bad week" in November

%py_november3%$ontext

phi_res(n,'pv',h)$(ord(h) > 7920 AND ord(h) < 8088)       = eps ;
phi_res(n,'wind_on',h)$(ord(h) > 7920 AND ord(h) < 8088)  = eps ;
phi_res(n,'wind_off',h)$(ord(h) > 7920 AND ord(h) < 8088) = eps ;

$ontext
$offtext

*******************************************************************************