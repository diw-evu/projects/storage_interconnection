***** SUPPLY SHARES ********************************************************************************

%py_all_shares%$ontext

con15a_share_demand_ror(n)
con15b_share_demand_rsvr(n)  
con15c_share_demand_phs(n)
con15d_share_demand_phs_open(n)
con15e_share_demand_wind_off(n)
con15f_share_demand_bio(n)
con15g_pp_ratio_phs(n)  
con15h_pp_ratio_phs_open(n)
con15i_ep_ratio_rsvr(n)    
con15j_ep_ratio_phs(n)     
con15k_ep_ratio_phs_open(n)

$ontext
$offtext

***** DEMAND SHARES ********************************************************************************

%py_demand_share_ror%$ontext

con15a_share_demand_ror

$ontext
$offtext

%py_demand_share_rsvr%$ontext

con15b_share_demand_rsvr
con15i_ep_ratio_rsvr

$ontext
$offtext

%py_demand_share_phs%$ontext

con15c_share_demand_phs
con15d_share_demand_phs_open

con15g_pp_ratio_phs
con15h_pp_ratio_phs_open

con15j_ep_ratio_phs
con15k_ep_ratio_phs_open

$ontext
$offtext

%py_demand_share_wind_off%$ontext

con15e_share_demand_wind_off

$ontext
$offtext

%py_demand_share_bio%$ontext

con15f_share_demand_bio

$ontext
$offtext
