* ---------------------------------------------------------------------------- *
***** Capacity Shares *****
* ---------------------------------------------------------------------------- *

Parameter

share_ror
share_rsvr
share_phs
share_phs_open
share_wind_off
share_bio

ratio_pp_phs
ratio_pp_phs_open
ratio_ep_rsvr
ratio_ep_phs
ratio_ep_phs_open
; 

share_ror          = 0;                
share_rsvr         = 0;      
share_phs          = 0;     
share_phs_open     = 0;          
share_wind_off     = 0;          
share_bio          = 0;     
    
ratio_pp_phs       = 0;        
ratio_pp_phs_open  = 0;             
ratio_ep_rsvr      = 0;         
ratio_ep_phs       = 0;        
ratio_ep_phs_open  = 0;             

con15a_share_demand_ror(n)..      N_TECH(n,'ror')           =E= share_ror      * 0.001 * sum(h, d(n,h))            ;
con15b_share_demand_rsvr(n)..     N_RSVR_P(n,'rsvr')        =E= share_rsvr     * 0.001 * sum(h, d(n,h))            ;
con15c_share_demand_phs(n)..      N_STO_P_OUT(n,'phs')      =E= share_phs      * 0.001 * sum(h, d(n,h))            ;
con15d_share_demand_phs_open(n).. N_STO_P_OUT(n,'phs_open') =E= share_phs_open * 0.001 * sum(h, d(n,h))            ;
con15e_share_demand_wind_off(n).. N_TECH(n,'wind_off')      =E= share_wind_off * 0.001 * sum(h, d(n,h))            ;
con15f_share_demand_bio(n)..      N_TECH(n,'bio')           =E= share_bio      * 0.001 * sum(h, d(n,h))            ;
con15g_pp_ratio_phs(n)..          N_STO_P_IN(n,'phs')       =E= ratio_pp_phs           * N_STO_P_OUT(n,'phs')      ;
con15h_pp_ratio_phs_open(n)..     N_STO_P_IN(n,'phs_open')  =E= ratio_pp_phs_open      * N_STO_P_OUT(n,'phs_open') ;
con15i_ep_ratio_rsvr(n)..         N_RSVR_E(n,'rsvr')        =E= ratio_ep_rsvr          * N_RSVR_P(n,'rsvr')        ;
con15j_ep_ratio_phs(n)..          N_STO_E(n,'phs')          =E= ratio_ep_phs           * N_STO_P_OUT(n,'phs')      ;
con15k_ep_ratio_phs_open(n)..     N_STO_E(n,'phs_open')     =E= ratio_ep_phs_open      * N_STO_P_OUT(n,'phs_open') ;

**********************************************************************************************************************************
