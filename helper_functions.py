import pandas as pd
from itertools import combinations

def get_factor_data(df):
    """_summary_

    Args:
        df (_type_): _description_

    Returns:
        _type_: _description_
    """

    # Aggregate over all countries (sum)
    df_agg = df.groupby(by = ['custom_name','tech','year'], as_index=False).agg({'value': 'sum', 'custom_name': 'first', 'scen_nr': 'first', 'custom_scen':'first'})

    # Aggregate over all year (mean)
    # N_STO_E_agg = N_STO_E_agg.groupby(by = ['custom_name','tech'],        as_index=False).agg({'value': 'mean', 'custom_name': 'first', 'scen_nr': 'first', 'custom_scen':'first'})

    # Remove zero values
    df_agg = df_agg[df_agg['value'] != 0]

    # Define new custom name variable
    df_agg['custom_name2'] = df_agg['custom_name'].str[3:]
    
    data = df_agg.copy()
    
    # Get values
    
    scenarios = data.custom_scen.unique()
    years     = data.year.unique()

    d = {}

    for y in years:
        d[y] = {}
        for i in scenarios:
            d[y][i] = data.query("custom_scen == @i & year == @y").value.values
    
    f_hat1 = {}

    for y in years:

        f_hat1[y] = {}

        for i in list(combinations(range(1,7),1)):
            full_number = int(''.join(map(str,i)))
            f_hat1[y][full_number] = pd.DataFrame(data = {
                'value' : d[y][full_number] - d[y][0],
                'tech ' : data.query("custom_scen == 0            & year == @y").tech.values, 
                'effect': data.query("custom_scen == @full_number & year == @y").custom_name2.unique()[0]})
    
    f_hat2 = {}

    for y in years:

        f_hat2[y] = {}

        for i in list(combinations(range(1,7),2)):
            full_number = int(''.join(map(str,i)))
            n1          = int("".join(map(str,list(combinations(i,1))[0])))
            n2          = int("".join(map(str,list(combinations(i,1))[1])))
            f_hat2[y][full_number] = pd.DataFrame(data = {
                'value' : d[y][full_number] - (d[y][n1] + d[y][n2]) + d[y][0], 
                'tech'  : data.query("custom_scen == 0            & year == @y").tech.values, 
                'effect': data.query("custom_scen == @full_number & year == @y").custom_name2.unique()[0]})

    f_hat3 = {}

    for y in years:

        f_hat3[y] = {}

        for i in list(combinations(range(1,7),3)):
            full_number = int(''.join(map(str,i)))
            n1          = int("".join(map(str,list(combinations(i,1))[0])))
            n2          = int("".join(map(str,list(combinations(i,1))[1])))
            n3          = int("".join(map(str,list(combinations(i,1))[2])))
            n11         = int("".join(map(str,list(combinations(i,2))[0])))
            n22         = int("".join(map(str,list(combinations(i,2))[1])))
            n33         = int("".join(map(str,list(combinations(i,2))[2])))
            f_hat3[y][''.join(map(str,i))] = pd.DataFrame(data = {
                'value' : d[y][full_number] - (d[y][n11] + d[y][n22] + d[y][n33]) + (d[y][n1] + d[y][n2] + d[y][n3]) - d[y][0], 
                'tech'  : data.query("custom_scen == 0            & year == @y").tech.values, 
                'effect': data.query("custom_scen == @full_number & year == @y").custom_name2.unique()[0]})

    f_hat4 = {}

    for y in years:

        f_hat4[y] = {}

        for i in list(combinations(range(1,7),4)):

            full_number = int(''.join(map(str,i)))
            n1          = int("".join(map(str,list(combinations(i,1))[0])))
            n2          = int("".join(map(str,list(combinations(i,1))[1])))
            n3          = int("".join(map(str,list(combinations(i,1))[2])))
            n4          = int("".join(map(str,list(combinations(i,1))[3])))
            n11         = int("".join(map(str,list(combinations(i,2))[0])))
            n22         = int("".join(map(str,list(combinations(i,2))[1])))
            n33         = int("".join(map(str,list(combinations(i,2))[2])))
            n44         = int("".join(map(str,list(combinations(i,2))[3])))
            n55         = int("".join(map(str,list(combinations(i,2))[4])))
            n66         = int("".join(map(str,list(combinations(i,2))[5])))
            n111        = int("".join(map(str,list(combinations(i,3))[0])))
            n222        = int("".join(map(str,list(combinations(i,3))[1])))
            n333        = int("".join(map(str,list(combinations(i,3))[2])))
            n444        = int("".join(map(str,list(combinations(i,3))[3])))

            values_calculated = d[y][full_number] \
                - (d[y][n111] + d[y][n222] + d[y][n333]+ d[y][n444]) \
                + (d[y][n11]  + d[y][n22]  + d[y][n33] + d[y][n44] + d[y][n55] + d[y][n66]) \
                - (d[y][n1]   + d[y][n2]   + d[y][n3]  + d[y][n4]) \
                +  d[y][0]

            f_hat4[y][''.join(map(str,i))] = pd.DataFrame(data = {
                'value': values_calculated,
                'tech' : data.query("custom_scen == 0             & year == @y").tech.values, 
                'effect': data.query("custom_scen == @full_number & year == @y").custom_name2.unique()[0],})

    f_hat5 = {}

    for y in years:
        
        f_hat5[y] = {}

        for i in list(combinations(range(1,7),5)):

            full_number = int(''.join(map(str,i)))
            n1          = int("".join(map(str,list(combinations(i,1))[0])))
            n2          = int("".join(map(str,list(combinations(i,1))[1])))
            n3          = int("".join(map(str,list(combinations(i,1))[2])))
            n4          = int("".join(map(str,list(combinations(i,1))[3])))
            n5          = int("".join(map(str,list(combinations(i,1))[4])))
            n11         = int("".join(map(str,list(combinations(i,2))[0])))
            n22         = int("".join(map(str,list(combinations(i,2))[1])))
            n33         = int("".join(map(str,list(combinations(i,2))[2])))
            n44         = int("".join(map(str,list(combinations(i,2))[3])))
            n55         = int("".join(map(str,list(combinations(i,2))[4])))
            n66         = int("".join(map(str,list(combinations(i,2))[5])))
            n77         = int("".join(map(str,list(combinations(i,2))[6])))
            n88         = int("".join(map(str,list(combinations(i,2))[7])))
            n99         = int("".join(map(str,list(combinations(i,2))[8])))
            n10         = int("".join(map(str,list(combinations(i,2))[9])))
            n111        = int("".join(map(str,list(combinations(i,3))[0])))
            n222        = int("".join(map(str,list(combinations(i,3))[1])))
            n333        = int("".join(map(str,list(combinations(i,3))[2])))
            n444        = int("".join(map(str,list(combinations(i,3))[3])))
            n555        = int("".join(map(str,list(combinations(i,3))[4])))
            n666        = int("".join(map(str,list(combinations(i,3))[5])))
            n777        = int("".join(map(str,list(combinations(i,3))[6])))
            n888        = int("".join(map(str,list(combinations(i,3))[7])))
            n999        = int("".join(map(str,list(combinations(i,3))[8])))
            n100        = int("".join(map(str,list(combinations(i,3))[9])))
            n1111       = int("".join(map(str,list(combinations(i,4))[0])))
            n2222       = int("".join(map(str,list(combinations(i,4))[1])))
            n3333       = int("".join(map(str,list(combinations(i,4))[2])))
            n4444       = int("".join(map(str,list(combinations(i,4))[3])))
            n5555       = int("".join(map(str,list(combinations(i,4))[4])))

            values_calculated = d[y][full_number] \
                - (d[y][n1111] + d[y][n2222] + d[y][n3333] + d[y][n4444] + d[y][n5555]) \
                + (d[y][n111]  + d[y][n222]  + d[y][n333]  + d[y][n444]  + d[y][n555] + d[y][n666] + d[y][n777] + d[y][n888] + d[y][n999] + d[y][n100]) \
                - (d[y][n11]   + d[y][n22]   + d[y][n33]   + d[y][n44]   + d[y][n55]  + d[y][n66]  + d[y][n77]  + d[y][n88]  + d[y][n99]  + d[y][n10]) \
                + (d[y][n1]    + d[y][n2]    + d[y][n3]    + d[y][n4]    + d[y][n5]) \
                -  d[y][0]

            f_hat5[y][full_number] = pd.DataFrame(data = {
                'value':  values_calculated,
                'tech' :  data.query("custom_scen == 0             & year == @y").tech.values, 
                'effect': data.query("custom_scen == @full_number & year == @y").custom_name2.unique()[0]})
    
    f_hat6 = {}

    for y in years:

        f_hat6[y] = {}

        for i in list(combinations(range(1,7),6)):

            full_number = int(''.join(map(str,i)))

            n1          = int("".join(map(str,list(combinations(i,1))[0])))
            n2          = int("".join(map(str,list(combinations(i,1))[1])))
            n3          = int("".join(map(str,list(combinations(i,1))[2])))
            n4          = int("".join(map(str,list(combinations(i,1))[3])))
            n5          = int("".join(map(str,list(combinations(i,1))[4])))
            n6          = int("".join(map(str,list(combinations(i,1))[5])))
            n01         = int("".join(map(str,list(combinations(i,2))[0])))
            n02         = int("".join(map(str,list(combinations(i,2))[1])))
            n03         = int("".join(map(str,list(combinations(i,2))[2])))
            n04         = int("".join(map(str,list(combinations(i,2))[3])))
            n05         = int("".join(map(str,list(combinations(i,2))[4])))
            n06         = int("".join(map(str,list(combinations(i,2))[5])))
            n07         = int("".join(map(str,list(combinations(i,2))[6])))
            n08         = int("".join(map(str,list(combinations(i,2))[7])))
            n09         = int("".join(map(str,list(combinations(i,2))[8])))
            n10         = int("".join(map(str,list(combinations(i,2))[9])))
            n11         = int("".join(map(str,list(combinations(i,2))[10])))
            n12         = int("".join(map(str,list(combinations(i,2))[11])))
            n13         = int("".join(map(str,list(combinations(i,2))[12])))
            n14         = int("".join(map(str,list(combinations(i,2))[13])))
            n15         = int("".join(map(str,list(combinations(i,2))[14])))
            n010        = int("".join(map(str,list(combinations(i,3))[0])))
            n020        = int("".join(map(str,list(combinations(i,3))[1])))
            n030        = int("".join(map(str,list(combinations(i,3))[2])))
            n040        = int("".join(map(str,list(combinations(i,3))[3])))
            n050        = int("".join(map(str,list(combinations(i,3))[4])))
            n060        = int("".join(map(str,list(combinations(i,3))[5])))
            n070        = int("".join(map(str,list(combinations(i,3))[6])))
            n080        = int("".join(map(str,list(combinations(i,3))[7])))
            n090        = int("".join(map(str,list(combinations(i,3))[8])))
            n100        = int("".join(map(str,list(combinations(i,3))[9])))
            n110        = int("".join(map(str,list(combinations(i,3))[10])))
            n120        = int("".join(map(str,list(combinations(i,3))[11])))
            n130        = int("".join(map(str,list(combinations(i,3))[12])))
            n140        = int("".join(map(str,list(combinations(i,3))[13])))
            n150        = int("".join(map(str,list(combinations(i,3))[14])))
            n160        = int("".join(map(str,list(combinations(i,3))[15])))
            n170        = int("".join(map(str,list(combinations(i,3))[16])))
            n180        = int("".join(map(str,list(combinations(i,3))[17])))
            n190        = int("".join(map(str,list(combinations(i,3))[18])))
            n200        = int("".join(map(str,list(combinations(i,3))[19])))
            n0100       = int("".join(map(str,list(combinations(i,4))[0])))
            n0200       = int("".join(map(str,list(combinations(i,4))[1])))
            n0300       = int("".join(map(str,list(combinations(i,4))[2])))
            n0400       = int("".join(map(str,list(combinations(i,4))[3])))
            n0500       = int("".join(map(str,list(combinations(i,4))[4])))
            n0600       = int("".join(map(str,list(combinations(i,4))[5])))
            n0700       = int("".join(map(str,list(combinations(i,4))[6])))
            n0800       = int("".join(map(str,list(combinations(i,4))[7])))
            n0900       = int("".join(map(str,list(combinations(i,4))[8])))
            n1000       = int("".join(map(str,list(combinations(i,4))[9])))
            n1100       = int("".join(map(str,list(combinations(i,4))[10])))
            n1200       = int("".join(map(str,list(combinations(i,4))[11])))
            n1300       = int("".join(map(str,list(combinations(i,4))[12])))
            n1400       = int("".join(map(str,list(combinations(i,4))[13])))
            n1500       = int("".join(map(str,list(combinations(i,4))[14])))
            n01000      = int("".join(map(str,list(combinations(i,5))[0])))
            n02000      = int("".join(map(str,list(combinations(i,5))[1])))
            n03000      = int("".join(map(str,list(combinations(i,5))[2])))
            n04000      = int("".join(map(str,list(combinations(i,5))[3])))
            n05000      = int("".join(map(str,list(combinations(i,5))[4])))
            n06000      = int("".join(map(str,list(combinations(i,5))[5])))

            values_calculated = d[y][full_number] \
            - (d[y][n01000] + d[y][n02000] + d[y][n03000] + d[y][n04000] + d[y][n05000] + d[y][n06000]) \
            + (d[y][n0100]  + d[y][n0200]  + d[y][n0300]  + d[y][n0400]  + d[y][n0500]  + d[y][n0600] + d[y][n0700] + d[y][n0800] + d[y][n0900] + d[y][n1000] + d[y][n1100] + d[y][n1200] + d[y][n1300] + d[y][n1400] + d[y][n1500]) \
            - (d[y][n010]   + d[y][n020]   + d[y][n030]   + d[y][n040]   + d[y][n050]   + d[y][n060]  + d[y][n070]  + d[y][n080]  + d[y][n090]  + d[y][n100]  + d[y][n110]  + d[y][n120]  + d[y][n130]  + d[y][n140]  + d[y][n150] + d[y][n160] + d[y][n170] + d[y][n180] + d[y][n190] + d[y][n200]) \
            + (d[y][n01]    + d[y][n02]    + d[y][n03]    + d[y][n04]    + d[y][n05]    + d[y][n06]   + d[y][n07]   + d[y][n08]   + d[y][n09]   + d[y][n10]   + d[y][n11]   + d[y][n12]   + d[y][n13]   + d[y][n14]   + d[y][n15]) \
            - (d[y][n1]     + d[y][n2]     + d[y][n3]     + d[y][n4]     + d[y][n5]     + d[y][n6]) \
            +  d[y][0]

            f_hat6[y][full_number] = pd.DataFrame(data = {
                'value'  : values_calculated,
                'tech'   : data.query("custom_scen == 0            & year == @y").tech.values,
                'effect' : data.query("custom_scen == @full_number & year == @y").custom_name2.unique()[0]})

    pd.options.mode.chained_assignment = None

    f_hat    = {}

    for y in years:
        f_hat[y] = {**f_hat1[y], **f_hat2[y], **f_hat3[y], **f_hat4[y], **f_hat5[y], **f_hat6[y]}
    f_hat_dict = {}

    for year in years:
        f_hat_dict[year]       = pd.concat(f_hat[year].values(), ignore_index=True)
        f_hat_dict[year].columns = ['value', 'tech1', 'effect', 'tech2']
        f_hat_dict[year]['tech'] = f_hat_dict[year]['tech2']
        f_hat_dict[year]['tech'][f_hat_dict[year]['tech2'].isna()] = f_hat_dict[year]['tech1'][f_hat_dict[year]['tech2'].isna()]  
        f_hat_dict[year] = f_hat_dict[year][['value','tech','effect']]
        f_hat_dict[year]["year"] = year

    f_hat_df = pd.concat(f_hat_dict.values(), ignore_index=True)
    
    f_hat_df.loc[f_hat_df['effect'].str.contains('NTC'), 'NTC']     = 1
    f_hat_df.loc[f_hat_df['effect'].str.contains('pv'),  'pv']      = 1
    f_hat_df.loc[f_hat_df['effect'].str.contains('wind'), 'wind']   = 1
    f_hat_df.loc[f_hat_df['effect'].str.contains('load'), 'load']   = 1
    f_hat_df.loc[f_hat_df['effect'].str.contains('hydro'), 'hydro'] = 1
    f_hat_df.loc[f_hat_df['effect'].str.contains('bio'), 'bio']     = 1

    f_hat_df['tech']     = f_hat_df['tech'].astype(str)
    f_hat_df             = f_hat_df.fillna(0)

    f_hat_df['n_factors'] = f_hat_df[['NTC','pv','wind','load','hydro','bio']].sum(axis=1) - 1
    f_hat_df['n_factors'] = f_hat_df['n_factors'].replace(0, 1)
    f_hat_df['value_adj'] = f_hat_df['value'] / f_hat_df['n_factors']

    sep_factors_years = {}

    for year in years:
        
        total        = pd.DataFrame(data = {'value': d[year][123456] - d[year][0],     'tech': data.query("custom_scen == 0 & year == @year").tech.values,  'effect': 'total'})
        interest     = pd.DataFrame(data = {'value': d[year][123456] - d[year][23456], 'tech': data.query("custom_scen == 0 & year == @year").tech.values,  'effect': 'interest'})

        f_hat_df_cp = f_hat_df.copy().query("year == @year")

        NTC_involved = pd.DataFrame(data = {'value' : f_hat_df_cp.query("NTC == 1").groupby('tech').value.sum().values,
                                            'tech'  : data.query("custom_scen == 0 & year == @year").tech.values,
                                            'effect': 'NTC-involved'})
        ntc_wind = pd.DataFrame(data = {'value'  : f_hat_df_cp.query("NTC == 1 & wind == 1").groupby('tech').value_adj.sum().values,
                                         'tech'  : data.query("custom_scen == 0 & year == @year").tech.values,
                                         'effect': 'Wind'})
        ntc_pv = pd.DataFrame(data = {'value' : f_hat_df_cp.query("NTC == 1 & pv == 1").groupby('tech').value_adj.sum().values,
                                      'tech'  : data.query("custom_scen == 0 & year == @year").tech.values,
                                      'effect': 'PV'})
        ntc_load = pd.DataFrame(data = {'value': f_hat_df_cp.query("NTC == 1 & load == 1").groupby('tech').value_adj.sum().values,
                                        'tech': data.query("custom_scen == 0 & year == @year").tech.values,
                                        'effect': 'Load'})
        ntc_hydro = pd.DataFrame(data = {'value': f_hat_df_cp.query("NTC == 1 & hydro == 1").groupby('tech').value_adj.sum().values,
                                         'tech': data.query("custom_scen == 0 & year == @year").tech.values,
                                         'effect': 'Hydro'})
        ntc_bio = pd.DataFrame(data = {'value': f_hat_df_cp.query("NTC == 1 & bio == 1").groupby('tech').value_adj.sum().values,
                                       'tech': data.query("custom_scen == 0 & year == @year").tech.values,
                                       'effect': 'Bio'})
        
        sep_factors_years[year] = pd.concat([total, interest,NTC_involved,ntc_wind,
                                             ntc_pv,ntc_load,ntc_hydro,ntc_bio],
                                             axis = 0).reset_index(drop=True)
        sep_factors_years[year]['year'] = year

    sep_factors = pd.concat(sep_factors_years.values(), ignore_index=True)

    return sep_factors,f_hat_df